# Strings used in Opera Unite applications.
# Copyright (C) 2009 Opera Software ASA
# This file is distributed under the same license as Opera Unite applications.
# Anders Sjögren <anderss@opera.com>, Kenneth Maage <kennethm@opera.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-10-06 13:18+01:00\n"
"PO-Revision-Date: 2009-11-09 14:39+0100\n"
"Last-Translator: Anne Lilleholt\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"

#. Text inside the <img> tag for a friend's thumbnail/avatar
#: public_html/ctrl.js
msgid "{user}'s picture"
msgstr "{प्रयोगकर्ता} का चित्र"

#. Tooltip for a message shown to the owner but
#. not yet delivered because the recipient is not online
#: public_html/ctrl.js
msgid "Waiting for {user} to come online"
msgstr "{प्रयोगकर्ता} के ऑनलाइन आने की प्रतीक्षा"

#. Tooltip for the [x] control on a message
#. that is waiting to be delivered
#: public_html/ctrl.js
msgid "Cancel sending this message"
msgstr "इस संदेश को भेजना रद्द करें"

#. Control to collapse the friends list to fit in
#. the vertical space of the window
#: public_html/style.css
msgid "Collapse"
msgstr "संकुचित"

#. main html page <title></title> element
#: templates/index.html
msgid "Messenger"
msgstr "मैसेंजर"

#. Stopped Messenger application error message overlay
#. Line 1 of 5
#: templates/index.html
msgid "Contact with Messenger interrupted."
msgstr "Messenger के साथ संपर्क बाधित हुआ."

#. Stopped Messenger application error message overlay
#. Line 2 of 5
#: templates/index.html
msgid "Please restart Messenger from the Unite panel."
msgstr "कृपया Unite पैनल से Messenger पुन:प्रारंभ करें."

#. Stopped Messenger application error message overlay
#. Line 3 of 5
#: templates/index.html
msgid "Open the Unite panel"
msgstr "Unite पैनल खोलें"

#. Stopped Messenger application error message overlay
#. Line 4 of 5
#: templates/index.html
msgid "Right click Messenger"
msgstr "Messenger पर बाएं क्लिक करें"

#. Stopped Messenger application error message overlay
#. Line 5 of 5
#: templates/index.html
msgid "Select <strong>Start</strong>"
msgstr "<strong>आरंभ करें</strong> का चयन करें"

#. Header used in Friends area before any friends
#. are added
#: templates/index.html
msgid "Get started"
msgstr "आरंभ करें"

#. Message shown while retrieving friend data
#: templates/index.html
msgid "Loading…"
msgstr "लोड कर रहा है..."

#. Header and control for showing the Help page
#: templates/index.html
msgid "Help"
msgstr "मदद"

#. Date range filter line item
#. Clicking the control shows messages from that point onward
#: templates/index.html
msgid "Older messages:"
msgstr "अन्य संदेश"

#. Date range filter line item
#. Clicking the control shows messages from that point onward
#: templates/index.html
msgid "Today"
msgstr "आज"

#. Date range filter line item
#. Clicking the control shows messages from that point onward
#: templates/index.html
msgid "Yesterday"
msgstr "बीता कल"

#. Date range filter line item
#. Clicking the control shows messages from that point onward
#: templates/index.html
msgid "Last week"
msgstr "पिछले सप्ताह"

#. Date range filter line item
#. Clicking the control shows messages from that point onward
#: templates/index.html
msgid "Last month"
msgstr "पिछले माह"

#. Date range filter line item
#. Clicking the control shows messages from that point onward
#: templates/index.html
msgid "All"
msgstr "सभी"

#. Help page section 1 of 4
#. Describes how Messenger works
#: templates/index.html
msgid "Use Messenger to communicate easily with your <A href=\"http://my.opera.com/{username}/friends/\" target=\"_blank\">My Opera friends</A>."
msgstr "अपने<A href=\"http://my.opera.com/{username}/friends/\" target=\"_blank\">मेरे Opera मित्र</A> के साथ आसानी से संप्रेषण करने के लिए Messenger प्रयोग करें."

#. Help page section 2 of 4 title
#. Describes how Messenger works
#: templates/index.html
msgid "Find friends"
msgstr "मित्र ढूंढें"

#. Help page section 2 of 4 description
#. Describes how Messenger works
#: templates/index.html
msgid "Find your friends on <A href=\"http://my.opera.com/{username}/friends/\" target=\"_blank\">my.opera.com »</A>"
msgstr "Find your friends on <A href=\"http://my.opera.com/{username}/friends/\" target=\"_blank\">my.opera.com »</A> पर अपने मित्र ढूंढें"

#. Help page section 3 of 4 title
#. Describes how Messenger works
#: templates/index.html
msgid "Install Messenger"
msgstr "Messenger स्थापित करें"

#. Help page section 3 of 4 description
#. Describes how Messenger works
#: templates/index.html
msgid "To use Messenger, you need to add friends on My Opera, and they must accept your friend requests. Your friends also need to have <A href=\"http://unite.opera.com/application/412/\" target=\"_blank\">Messenger</A> installed."
msgstr "Messenger प्रयोग करने के लिए, आपको मेरा Opera में मित्र जोड़ने की आवश्यकता होती है और उन्हें आपका मित्र अनुरोध अवश्य स्वीकार करना होगा. आपके मित्रों को <A href=\"http://unite.opera.com/application/412/\" target=\"_blank\">Messenger</A> स्थापित करने की आवश्यकता होती है."

#. Help page section 4 of 4 title
#. Describes how Messenger works
#: templates/index.html
msgid "Get online together"
msgstr "एक साथ ऑनलाइन हों"

#. Help page section 4 of 4 description
#. Describes how Messenger works
#: templates/index.html
msgid "Your My Opera friends will be listed as offline until they have Messenger running. You can send messages to friends who are offline, and they will receive them when both of you are online."
msgstr "आपके मेरे Opera मित्रों को तब तक ऑफलाइन सूचीबद्ध किया जाएगा जब तक वे Messenger नहीं चलाते. आप उन मित्रों को संदेश भेज सकते हैं जो ऑफलाइन हैं और वे तब उन्हें प्राप्त करेंगे जब आप दोनों ऑनलाइन हों."

#. No javascript message
#: templates/index.html
msgid "You need JavaScript enabled to use this application."
msgstr "आपको इस अनुप्रयोग का प्रयोग करने के लिए JavaScript सक्षम करने की आवश्यकता है."

#. Tooltip for the control to insert smilies like :-) :-D etc
#: templates/index.html
msgid "Smilies"
msgstr "स्माइलीज़"

#. Button to submit the message being composed
#: templates/index.html
msgid "Send"
msgstr "भेजें"

#. Header text for friends section
#: templates/index.html
msgid "Friends"
msgstr "मित्र"

#. Message line showing the number of friend requests the owner has
#. Singular
#: templates/index.html
msgid "You have 1 friend request »"
msgstr "आपके पास 1 मित्र अनुरोध है »"

#. Message line showing the number of friend requests the owner has
#. Two or more
#: templates/index.html
msgid "You have {count} friend requests »"
msgstr "आपके पास {count} मित्र अनुरोध हैं »"

#. Line to point the owner to My Opera to search for friends
#. Careful handling the » html element
#: templates/index.html
msgid "Find your friends on <A href=\"http://my.opera.com/{username}/friends/\" target=\"_blank\">my.opera.com »</A>"
msgstr "<A href=\"http://my.opera.com/{username}/friends/\" target=\"_blank\">my.opera.com »</A> पर अपने मित्र खोजें"

#. Header text in the friends sidebar who have Messenger
#. running currently, and are available to chat
#: templates/index.html
msgid "Online"
msgstr "ऑनलाइन"

#. Message in Friends area when all friends are Offline
#: templates/index.html
msgid "All your My Opera friends are offline."
msgstr "आपके सभी मेरे Opera मित्र ऑफलाइन हैं."

#. Header text in the friends sidebar who do not
#. have Messenger running currently, so they are
#. not available to chat
#: templates/index.html
msgid "Offline"
msgstr "ऑफलाइन"

#. Message in Friends area when all friends are Online
#: templates/index.html
msgid "All your My Opera friends are online."
msgstr "आपके सभी मेरे Opera मित्र ऑनलाइन हैं."

#. Message in Friends area when the owner has no friends
#: templates/index.html
msgid "You have not yet added any My Opera friends.<BR>Find your friends on <A href=\"http://my.opera.com/{username}/friends/\">my.opera.com »</A>"
msgstr "आपने अभी तक मेरे Opera मित्र नहीं जोड़े हैं.<BR> <A href=\"http://my.opera.com/{username}/friends/\">my.opera.com »</A> पर अपने मित्र खोजें"

#. Welcome message to owner, first part
#. Example "Hi kmaage"
#: templates/index.html
msgid "Hi <SPAN id=\"username\">{username}</SPAN>"
msgstr "Hi <SPAN id=\"username\">{username}</SPAN>"

#. Message shown when no friends could be retrieved
#. from my opera. Part 1 of 5
#: templates/index.html
msgid "No friends were imported from My Opera."
msgstr "मेरा Opera  से मित्रों को आयात नहीं किया गया था."

#. Message shown when no friends could be retrieved
#. from my opera. Part 2 of 5
#. Control to attempt the import again
#: templates/index.html
msgid "Retry…"
msgstr "फिर से प्रयास करें..."

#. Message shown when no friends could be retrieved
#. from my opera. Part 3 of 5
#: templates/index.html
msgid "You do not seem to have added any My Opera friends."
msgstr "लगता है आपने कोई मेरे Opera मित्र नहीं जोड़े हैं."

#. Message shown when no friends could be retrieved
#. from my opera. Part 4 of 5
#: templates/index.html
msgid "Invite your friends to join My Opera by <A href=\"http://my.opera.com/{username}/friends/invite/\" target=\"_blank\">sending them e-mail</A>."
msgstr "अपने मित्रों को मेरा Opera से जुड़ने के लिए <A href=\"http://my.opera.com/{username}/friends/invite/\" target=\"_blank\">उन्हें ई-मेल भेजकर</A> द्वारा आमंत्रित करें."

#. Message shown when no friends could be retrieved
#. from my opera. Part 5 of 5
#: templates/index.html
msgid "No friends were imported from My Opera."
msgstr "मेरा Opera से मित्रों को आयात नहीं किया गया था."

#. Control for expanding the list of friends to show them all
#: templates/index.html
msgid "Show All"
msgstr "सभी दिखाएं"

#. Message to visitors who lack Messenger
#. Part 1 of 3
#: templates/index_noaccess.html
#: templates/index_noaccess_opera.html
#: templates/index_noaccess_unite.html
msgid "{username} is using <A href=\"http://unite.opera.com/application/412/\">Messenger</A> on Opera Unite."
msgstr "{प्रयोगकर्तानाम} Opera Unite पर <A href=\"http://unite.opera.com/application/412/\">Messenger</A> का प्रयोग कर रहा है."

#. Message to visitors who have neither Opera nor Unite
#. Part 2 of 3
#: templates/index_noaccess.html
msgid "Interested in Opera Unite? Opera includes built-in applications that make it easy to share data and communicate directly with your friends. <A href=\"unite.opera.com\">Learn more</A>"
msgstr "Opera Unite में रूचि है? Opera में अंतर्निहित अनुप्रयोग हैं जो इसे डेटा साझा करने और अपने मित्रों से सीधे ही संप्रेषण करने में आसान बनाते हैं. <A href=\"unite.opera.com\">अधिक जानें</A>"

#. Message to visitors who have Opera, but not Unite
#. Part 2 of 3
#: templates/index_noaccess_opera.html
msgid "Already using Opera? Upgrade to Opera 10.10 and Messenger is included."
msgstr "पहले ही Opera प्रयोग कर रहा है? Opera 10.10 से नवीनीकृत करें और Messenger शामिल होता है."

#. Message to visitors who have Opera Unite, but not Messenger
#. Part 2a of 3
#. Alt text for screenshot of how to start Messenger
#: templates/index_noaccess_unite.html
msgid "Instructions for launching Messenger"
msgstr "Messenger लॉन्च करने के निर्देश"

#. Message to visitors who have Opera Unite, but not Messenger
#. Part 2b of 3
#: templates/index_noaccess_unite.html
msgid "Already using Opera Unite? Use Messenger to exchange messages with {username}. <A href=\"http://my.opera.com/{username}/addfriend.pl\">Become friends »</A>"
msgstr "पहले ही Opera Unite का प्रयोग कर रहे हैं? {प्रयोगकर्तानाम} के साथ संदेश बदलने के लिए Messenger प्रयोग करें. <A href=\"http://my.opera.com/{username}/addfriend.pl\">मित्र बने »</A>"

#. Message to visitors who lack Messenger
#. Part 3 of 3
#: templates/index_noaccess.html
#: templates/index_noaccess_opera.html
#: templates/index_noaccess_unite.html
msgid "<A href=\"http://unite.opera.com/application/412/\">Tell me more about Messenger</A>."
msgstr "<A href=\"http://unite.opera.com/application/412/\">मुझे Messenger के बारे में बताएं</A>."

#. Status line showing the number of unread messages
#. the owner has. Singular
#: PresenceChat.js
msgid "You have 1 new message"
msgstr "आपके पास 1 नया संदेश है"

#. Status line showing the number of unread messages
#. the owner has. More than 1
#: PresenceChat.js
msgid "You have {count} new messages"
msgstr "आपके पास {संख्या} नए संदेश हैं"

#. Status line showing the number of unread messages
#. the owner has. None.
#: PresenceChat.js
msgid "You have no new messages"
msgstr "आपके पास नए संदेश नहीं हैं"

#. Footer, common to many Opera Unite Apps
#: index.html
msgid "Powered by <A href=\"http://unite.opera.com/\">Opera Unite</A>"
msgstr "<A href=\"http://unite.opera.com/\">Opera Unite</A> द्वारा पावर्ड"

